MODELING_THIRDPARTY_PRODUCT("tcl" "" "tcl.h" "tcl86")
if (3RDPARTY_tcl_DIR AND NOT 3RDPARTY_tk_DIR)
  if (EXISTS ${3RDPARTY_tcl_INCLUDE_DIR}/tk.h)
    set (3RDPARTY_tk_DIR "${3RDPARTY_tcl_DIR}" CACHE PATH "The directory containing tk" FORCE)
  endif()
endif()
MODELING_THIRDPARTY_PRODUCT("tk" "" "tk.h" "tk86")

MODELING_THIRDPARTY_PRODUCT("freeimage" "" "FreeImage.h" "FreeImage")
if (3RDPARTY_freeimage_DIR AND NOT 3RDPARTY_freeimageplus_DIR)
  set (3RDPARTY_freeimageplus_DIR "${3RDPARTY_freeimage_DIR}" CACHE PATH "The directory containing freeimageplus" FORCE)
endif()
MODELING_THIRDPARTY_PRODUCT("freeimageplus" "" "FreeImagePlus.h" "FreeImagePlus")

MODELING_THIRDPARTY_PRODUCT("freetype" "" "ft2build.h" "freetype")
MODELING_THIRDPARTY_PRODUCT("gl2ps" "" "gl2ps.h" "gl2ps")

#--------------------------------------------------------------------------
# Installation
if (WIN32)

  # Tcl
  install (FILES ${3RDPARTY_tcl_DIR}/bin/tcl86.dll DESTINATION bin)
  install (FILES ${3RDPARTY_tcl_DIR}/bin/zlib1.dll DESTINATION bin)

endif()
