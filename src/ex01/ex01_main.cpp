//-----------------------------------------------------------------------------
// Created on: 18 October 2016
// www: https://sites.google.com/site/modelingpractice/
//-----------------------------------------------------------------------------

// octest includes
#include "ex01_commands.h"

// OCCT includes
#include <Draw.hxx>
#include <DBRep.hxx>
#include <DrawTrSurf.hxx>
#include <GeomliteTest.hxx>
#include <GeometryTest.hxx>
#include <BRepTest.hxx>

//-----------------------------------------------------------------------------

//! This function has a conventional Draw_InitAppli name. It will be called
//! automatically from the main() function defined by DRAW_MAIN macro. Use
//! this callback function to initialize your Draw interpreter.
//! \param di [in] Draw Interpreter instance.
void Draw_InitAppli(Draw_Interpretor& di)
{
  // Add basic DRAW commands
  Draw::Commands(di);
  DBRep::BasicCommands(di);
  DrawTrSurf::BasicCommands(di);

  // Add geometry and topology commands
  GeomliteTest::AllCommands(di);
  GeometryTest::AllCommands(di);
  BRepTest::AllCommands(di);

  // Add custom commands
  ex01_commands(di);
}

// Include DRAW standard 'main' procedure
#include <Draw_Main.hxx>
DRAW_MAIN
