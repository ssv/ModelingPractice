//-----------------------------------------------------------------------------
// Created on: 18 October 2016
// www: https://sites.google.com/site/modelingpractice/
//-----------------------------------------------------------------------------

// Own include
#include "ex01_commands.h"

// OCCT includes
#include <BRep_Tool.hxx>
#include <BRepAlgoAPI_Cut.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepFilletAPI_MakeChamfer.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepPrimAPI_MakeRevol.hxx>
#include <BRepTools.hxx>
#include <DBRep.hxx>
#include <DrawTrSurf.hxx>
#include <GCE2d_MakeSegment.hxx>
#include <Geom2d_Line.hxx>
#include <GeomAPI.hxx>
#include <gp_Pln.hxx>
#include <TCollection_AsciiString.hxx>
#include <TopExp.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>

// STL includes
#include <vector>

//-----------------------------------------------------------------------------

static int Test(Draw_Interpretor& di, int argc, const char** argv)
{
  if ( argc != 1 )
  {
    std::cout << "Error: " << argv[0] << " - invalid number of arguments" << std::endl;
    std::cout << "Usage: type help " << argv[0] << std::endl;
    return 1; // TCL_ERROR
  }

  //-------------------------------------------------------
  // Stage 1: create reper points
  //-------------------------------------------------------

  std::vector<gp_Pnt2d> P =
  {
    gp_Pnt2d(0, 0),
    gp_Pnt2d(200, 0),
    gp_Pnt2d(200, 20),
    gp_Pnt2d(145, 20),
    gp_Pnt2d(115, 40),
    gp_Pnt2d(115, 70),
    gp_Pnt2d(65, 70),
    gp_Pnt2d(65, 20),
    gp_Pnt2d(45, 20),
    gp_Pnt2d(45, 30),
    gp_Pnt2d(15, 30),
    gp_Pnt2d(15, 20),
    gp_Pnt2d(0, 20)
  };

  //-------------------------------------------------------
  // Stage 2: create two-dimensional curves
  //-------------------------------------------------------

  std::vector<Handle(Geom2d_Curve)> curves;
  for ( size_t i = 0; i < P.size(); ++i )
  {
    {
      TCollection_AsciiString name("P_"); name += (int) i;
      Standard_CString cname = name.ToCString();
      DrawTrSurf::Set(cname, P[i]);
    }

    const gp_Pnt2d& P_curr = P[i];
    const gp_Pnt2d& P_next = P[i == (P.size() - 1) ? 0 : (i + 1)];

    Handle(Geom2d_TrimmedCurve)
      curve = ( i == (P.size() - 1) ) ?
        GCE2d_MakeSegment(P_next, P_curr) :
        GCE2d_MakeSegment(P_curr, P_next);
    //
    curves.push_back(curve);

    {
      TCollection_AsciiString name("c_"); name += (int) i;
      Standard_CString cname = name.ToCString();
      DrawTrSurf::Set(cname, curve);
    }
  }

  //-------------------------------------------------------
  // Stage 3: create three-dimensional curves
  //-------------------------------------------------------

  std::vector<Handle(Geom_Curve)> curves3d;
  gp_Pln S( gp::Origin(), gp::DZ() );
  for ( size_t i = 0; i < curves.size(); ++i )
  {
    Handle(Geom_Curve)
      c3d = GeomAPI::To3d(curves[i], S);
    //
    curves3d.push_back(c3d);

    {
      TCollection_AsciiString name("c3d_"); name += (int) i;
      Standard_CString cname = name.ToCString();
      DrawTrSurf::Set(cname, c3d);
    }
  }

  //-------------------------------------------------------
  // Stage 4: create face
  //-------------------------------------------------------

  BRepBuilderAPI_MakeWire mkWire;
  for ( size_t i = 0; i < curves3d.size(); ++i )
  {
    // TopoDS_Compound
    // TopoDS_Solid
    // TopoDS_Shell
    // TopoDS_Face
    // TopoDS_Wire
    // TopoDS_Edge
    // TopoDS_Vertex

    TopoDS_Edge e = BRepBuilderAPI_MakeEdge(curves3d[i]);
    //
    mkWire.Add(e);
  }
  TopoDS_Wire wire = mkWire.Wire();
  //
  di.Eval("clear");
  DBRep::Set("wire", wire);

  TopoDS_Face face = BRepBuilderAPI_MakeFace(S, wire);
  //
  DBRep::Set("face", face);

  //-------------------------------------------------------
  // Stage 5: create solid of revolution
  //-------------------------------------------------------

  gp_Ax1 revolAx( gp::Origin(), gp::DX() );
  BRepPrimAPI_MakeRevol mkRevol(face, revolAx, 2*M_PI);
  //
  if ( !mkRevol.IsDone() )
  {
    std::cout << "Error: cannot build solid of revolution" << std::endl;
    return 1;
  }
  //
  const TopoDS_Shape& solid = mkRevol.Shape();

  DBRep::Set("solid", solid);

  TopoDS_Edge edge;
  {
    TopTools_IndexedMapOfShape M;
    TopExp::MapShapes(solid, TopAbs_EDGE, M);
    edge = TopoDS::Edge( M(1) );
  }

  //TopoDS_Face face;
  TopTools_IndexedMapOfShape M;
  TopExp::MapShapes(solid, TopAbs_FACE, M);
  face = TopoDS::Face( M(1) );

  DBRep::Set("edge", edge);
  DBRep::Set("face", face);

  BRepFilletAPI_MakeChamfer mkChamfer(solid);
  mkChamfer.Add(5.0, edge, face);
  mkChamfer.Build();

  if ( !mkChamfer.IsDone() )
  {
    std::cout << "Error: cannot build chamfer" << std::endl;
    return 1;
  }
  TopoDS_Shape chamferedSolid = mkChamfer.Shape();

  //-------------------------------------------------------
  // Stage 6: create cylindrical tool to cut a hole
  //-------------------------------------------------------

  TopoDS_Face baseHoleFace = TopoDS::Face( M(5) );

  double uMin, uMax, vMin, vMax;
  BRepTools::UVBounds(baseHoleFace, uMin, uMax, vMin, vMax);
  //
  const double uMid = (uMin + uMax)*0.5;
  const double vMid = (vMin + vMax)*0.5;
  //
  gp_Pnt2d center2d(uMid, vMid);
  //
  di.Eval("clear");
  di.Eval("2dclear");
  DrawTrSurf::Set("center2d", center2d);
  DBRep::Set("s", chamferedSolid);

  Handle(Geom_Surface)
    hostSurface = BRep_Tool::Surface(baseHoleFace);
  //
  {
    gp_Pnt P;
    gp_Vec dS_dU, dS_dV;
    hostSurface->D1(uMid, vMid, P, dS_dU, dS_dV);
    //
    gp_Dir norm = dS_dU ^ dS_dV;
    //
    if ( baseHoleFace.Orientation() == TopAbs_REVERSED )
      norm.Reverse();

    DrawTrSurf::Set("center", P);

    const double r = 15.0;
    const double l = 55.0;

    gp_Ax2 placementPlane(P, norm.Reversed(), dS_dU);

    TopoDS_Shape tool = BRepPrimAPI_MakeCylinder(placementPlane, r, l);
    DBRep::Set("tool", tool);

    BRepAlgoAPI_Cut Cutter(chamferedSolid, tool);
    if ( !Cutter.IsDone() )
    {
      std::cout << "Error in Booleans" << std::endl;
      return 1;
    }

    TopoDS_Shape result = Cutter.Shape();
    //
    if ( result.ShapeType() == TopAbs_SOLID )
      std::cout << "Result is solid" << std::endl;

    di.Eval("clear; axo");
    DBRep::Set("result", result);
    di.Eval("fit");
  }

  // TODO

  return 0; //TCL_OK
}

//-----------------------------------------------------------------------------

void ex01_commands(Draw_Interpretor& di)
{
  // Parameters of Draw_Interpretor::Add(name, help, file, pointer, group):
  // <name>    - name of the command
  // <help>    - help for the command (displayed by 'help name')
  // <file>    - name of the source file (returned by 'getsource name')
  // <pointer> - pointer to the command function
  // <group>   - name of the group of commands

  const char *grp = "Modeling practice commands";

  di.Add("unntest", "this is help for unntest", __FILE__, Test, grp);
}
