//-----------------------------------------------------------------------------
// Created on: 02 December 2016
// www: https://sites.google.com/site/modelingpractice/
//-----------------------------------------------------------------------------

// Own include
#include "ex02_commands.h"

// OCCT includes
#include <BRep_Builder.hxx>
#include <BRep_Tool.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepTools.hxx>
#include <DBRep.hxx>
#include <DrawTrSurf.hxx>
#include <Geom_CylindricalSurface.hxx>
#include <math_BullardGenerator.hxx>
#include <math_PSO.hxx>
#include <math_PSOParticlesPool.hxx>
#include <ShapeAnalysis_Surface.hxx>
#include <STEPControl_Reader.hxx>
#include <TopExp_Explorer.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Compound.hxx>
#include <TopoDS_Face.hxx>

// STL includes
#include <string>
#include <vector>

//-----------------------------------------------------------------------------

//! Function for average distance between the point set and the target
//! cylindrical surface.
class SquaredDistFunc : public math_MultipleVarFunction
{
public:

  SquaredDistFunc(const std::vector<gp_Pnt>&             points,
                  const Handle(Geom_CylindricalSurface)& cylSurf)
  : math_MultipleVarFunction()
  {
    m_points  = points;
    m_surface = Handle(Geom_CylindricalSurface)::DownCast( cylSurf->Copy() );
  }

public:

  virtual int NbVariables() const
  {
    return 1;
  }

  virtual bool Value(const math_Vector& X, double& F)
  {
    // Apply new radius
    m_surface->SetRadius( X(1) );

    // Prepare analysis tool
    ShapeAnalysis_Surface sas(m_surface);

    // Calculate average distance
    double dist = 0;
    for ( size_t pidx = 0; pidx < m_points.size(); ++pidx )
    {
      sas.ValueOfUV(m_points[pidx], 1.0e-6);
      dist += Square( sas.Gap() );
    }

    // Set function value
    F = dist;
    return true;
  }

protected:

  std::vector<gp_Pnt>             m_points;  //!< Points to fit into.
  Handle(Geom_CylindricalSurface) m_surface; //!< Surface to fit.

};

//-----------------------------------------------------------------------------

static int GeneratePoints(Draw_Interpretor& di, int argc, const char** argv)
{
  if ( argc != 4 )
  {
    std::cout << "Error: " << argv[0] << " - invalid number of arguments" << std::endl;
    std::cout << "Usage: type help " << argv[0] << std::endl;
    return 1; // TCL_ERROR
  }

  /* ====================================
   *  STAGE 01: read CAD model from STEP
   * ==================================== */

  STEPControl_Reader Reader;
  IFSelect_ReturnStatus stat = Reader.ReadFile(argv[1]);
  //
  if ( stat != IFSelect_RetDone )
  {
    std::cout << "Cannot read STEP file " << argv[1] << std::endl;
    return 1;
  }

  // Transfer all roots into one shape or into several shapes
  Reader.TransferRoots();
  TopoDS_Shape model = Reader.OneShape();
  //
  DBRep::Set(argv[2], model);

  /* ============================
   *  STAGE 02: sample CAD model
   * ============================ */

  Handle(Geom_CylindricalSurface) cylSurface;
  double uMin = 0, uMax = 0, vMin = 0, vMax = 0;
  //
  for ( TopExp_Explorer exp(model, TopAbs_FACE); exp.More(); exp.Next() )
  {
    const TopoDS_Shape& subShape = exp.Current();

    // Downcast sub-shape of abstract type to face type
    TopoDS_Face face = TopoDS::Face(subShape);

    // Take host surface
    Handle(Geom_Surface) surf = BRep_Tool::Surface(face);
    //
    if ( surf->IsKind( STANDARD_TYPE(Geom_CylindricalSurface) ) )
    {
      // Get parametric bounds of the face
      BRepTools::UVBounds(face, uMin, uMax, vMin, vMax);

      // Initialize cylindrical surface
      cylSurface = Handle(Geom_CylindricalSurface)::DownCast(surf);
      break;
    }
  }

  if ( cylSurface.IsNull() )
  {
    std::cout << "Cylindrical surface not found" << std::endl;
    return 1;
  }

  std::vector<gp_Pnt> points;

  const double uStep = (uMax - uMin)*0.01;
  const double vStep = (vMax - vMin)*0.01;
  int idx = 0;

  math_BullardGenerator RNG;

  double v = vMin;
  while ( v < vMax )
  {
    double u = uMin;
    while ( u < uMax )
    {
      // Noised value
      const double uNoised = u + RNG.NextReal() / (uMax - uMin);
      const double vNoised = v + RNG.NextReal() / (vMax - vMin);

      // Evaluate
      gp_Pnt P;
      gp_Vec d1u, d1v;
      cylSurface->D1(uNoised, vNoised, P, d1u, d1v);

      // Noised normal
      const gp_Vec nNoised = (d1u^d1v).Normalized()*RNG.NextReal();

      // Noised point
      P = P.XYZ() + nNoised.XYZ();

      // Store
      points.push_back(P);
      u += uStep;
    }
    v += vStep;
  }

  /* ========================================================================
   *  STAGE 03: put points into a compound for storing them in Draw variable
   * ======================================================================== */

  TopoDS_Compound pointsComp;
  BRep_Builder BB;
  BB.MakeCompound(pointsComp);

  // Put points in a compound
  for ( size_t pidx = 0; pidx < points.size(); ++pidx )
  {
    BB.Add( pointsComp, BRepBuilderAPI_MakeVertex(points[pidx]) );
  }
  di.Eval("clear");
  DBRep::Set(argv[3], pointsComp);

  return 0;
}

//-----------------------------------------------------------------------------

static int FitCylinder(Draw_Interpretor& di, int argc, const char** argv)
{
  if ( argc != 4 )
  {
    std::cout << "Error: " << argv[0] << " - invalid number of arguments" << std::endl;
    std::cout << "Usage: type help " << argv[0] << std::endl;
    return 1;
  }

  /* =======================
   *  Stage 01: preparation
   * ======================= */

  TopoDS_Shape pointsShape = DBRep::Get(argv[1]);
  //
  if ( pointsShape.IsNull() || pointsShape.ShapeType() != TopAbs_COMPOUND )
  {
    std::cout << "Error: compound is expected as the first argument" << std::endl;
    return 1;
  }

  // Unpack compound to the collection of points
  std::vector<gp_Pnt> points;
  //
  for ( TopExp_Explorer exp(pointsShape, TopAbs_VERTEX); exp.More(); exp.Next() )
  {
    const TopoDS_Vertex& V = TopoDS::Vertex( exp.Current() );
    points.push_back( BRep_Tool::Pnt(V) );
  }

  // Get coarse cylinder
  TopoDS_Shape coarseCyl = DBRep::Get(argv[2]);

  // Take the cylindrical face
  Handle(Geom_CylindricalSurface) cylSurf;
  double uMin = 0, uMax = 0, vMin = 0, vMax = 0;
  //
  for ( TopExp_Explorer exp(coarseCyl, TopAbs_FACE); exp.More(); exp.Next() )
  {
    const TopoDS_Face& face = TopoDS::Face( exp.Current() );
    //
    Handle(Geom_Surface) surf = BRep_Tool::Surface(face);
    //
    if ( surf->IsKind( STANDARD_TYPE(Geom_CylindricalSurface) ) )
    {
      // Get parametric bounds of the face
      BRepTools::UVBounds(face, uMin, uMax, vMin, vMax);
      //
      cylSurf = Handle(Geom_CylindricalSurface)::DownCast(surf);
      break;
    }
  }

  if ( cylSurf.IsNull() )
  {
    std::cout << "Error: there is no cylindrical surface to fit" << std::endl;
    return 1; // Error
  }

  /* =================================
   *  Run particle swarm optimization
   * ================================= */

  // Inputs
  math_Vector rMin(1, 1), rMax(1, 1);
  rMin(1) = 0.0;
  rMax(1) = 100.0;
  //
  math_Vector rStep(1, 1), rDelta(1, 1);
  rStep = (rMax - rMin)*0.01;

  // Prepare objective function
  SquaredDistFunc distFunc(points, cylSurf);

  // Outputs
  math_Vector rOut(1, 1);

  // Run optimization
  double distance;
  math_PSO PSO(&distFunc, rMin, rMax, rStep);
  PSO.Perform(rStep, distance, rOut);

  std::cout << "Optimized radius: " << rOut(1) << std::endl;
  std::cout << "Best fitness (squared deviation): " << distance << std::endl;

  Handle(Geom_CylindricalSurface)
    res = Handle(Geom_CylindricalSurface)::DownCast( cylSurf->Copy() );
  //
  res->SetRadius( rOut(1) );

  DBRep::Set( argv[3], BRepBuilderAPI_MakeFace(res->Cylinder(), uMin, uMax, vMin, vMax) );

  return 0;
}

//-----------------------------------------------------------------------------

void ex02_commands(Draw_Interpretor& di)
{
  // Parameters of Draw_Interpretor::Add(name, help, file, pointer, group):
  // <name>    - name of the command
  // <help>    - help for the command (displayed by 'help name')
  // <file>    - name of the source file (returned by 'getsource name')
  // <pointer> - pointer to the command function
  // <group>   - name of the group of commands

  const char *grp = "Modeling practice commands";

  di.Add("generate_points",
         "generate_points step_filename result point_cloud",
         __FILE__,
         GeneratePoints,
         grp);

  di.Add("fit_cylinder",
         "fit_cylinder point_cloud coarse_cylinder tight_cyl",
         __FILE__,
         FitCylinder,
         grp);
}
