pload MODELING VISUALIZATION DATAEXCHANGE
generate_points D:/OCC/trainings/2016_UNN_OCCT_Course/octest/data/cyl.step cyl points
axo; fit

pcylinder coarse_cyl 2.6 50
fit_cylinder points coarse_cyl res
top; fit

vinit
vdisplay points
vdisplay coarse_cyl
vdisplay res
vsetcolor coarse_cyl red
vsetcolor res green
